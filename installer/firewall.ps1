param
(
    [string]$command = "Add"
)

Function Remove-FW() {
    &C:\Windows\System32\netsh.exe advfirewall firewall `
      delete rule name="Streambox Iris decoder"
}

Function Add-FW() {
    Remove-FW # prevent duplicates
    &C:\Windows\System32\netsh.exe advfirewall firewall add rule `
      name="Streambox Iris decoder" dir=in action=allow `
      program="C:\Program Files\Streambox\Iris\decoder.exe" `
      enable=yes profile=Any
}

switch($command){
    "Add" { Add-FW }
    "Remove" { Remove-FW }
}
