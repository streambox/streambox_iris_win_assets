Function Update-HashPath {
    param (
        [string]$SourceDirectory,
        [string]$DestDirectory
    )

    # Check if the source directory exists
    if (-not (Test-Path -Path $SourceDirectory -PathType Container)) {
        Write-Warning "Source directory $SourceDirectory does not exist, bailing out."
        return
    }

    # Check if the destination directory exists
    if (-not (Test-Path -Path $DestDirectory -PathType Container)) {
        Write-Warning "Destination directory $DestDirectory does not exist, bailing out."
        return
    }

    # Use regular expressions to find files with a 32-character hexadecimal hash in the source directory
    $fileRegex = "^$([regex]::Escape($SourceDirectory))\\([a-f0-9]{32})$"
    $filePaths = Get-ChildItem -Path $SourceDirectory -Recurse | Where-Object { $_.FullName -match $fileRegex } | Select-Object -ExpandProperty FullName

    # Copy each file to the appropriate destination directory with the same hash
    foreach ($filePath in $filePaths) {
        # Check if the source file exists
        if (-not (Test-Path -Path $filePath -PathType Leaf)) {
            Write-Warning "Source file $filePath does not exist."
            continue
        }

        $hashRegex = "[a-f0-9]{32}"
        $hashMatch = [regex]::Match($filePath, $hashRegex)
        $hash = $hashMatch.Value

        $destFilePath = Join-Path -Path $DestDirectory -ChildPath $hash

        # Check if the destination file exists
        if (Test-Path -Path $destFilePath -PathType Leaf) {
            Write-Host "Destination file $destFilePath already exists, skipping."
            continue
        }

        Copy-Item -Path $filePath -Destination $destFilePath
    }
}

# &"C:\Program Files\Streambox\Iris\nssm.exe" remove StreamboxIris confirm

try { $service = Get-Service -ErrorAction SilentlyContinue -Name StreamboxIris } catch {}
if (! $service) {
    &"C:\Program Files\Streambox\Iris\nssm.exe" install StreamboxIris "C:\Program Files\Streambox\Iris\decoder.exe"
}
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppDirectory "C:\ProgramData\Streambox\Iris"
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris Application "C:\Program Files\Streambox\Iris\decoder.exe"
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppStdout "C:\ProgramData\Streambox\Iris\log\iris.log"
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppStderr "C:\ProgramData\Streambox\Iris\log\iris.log"
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris Description "Streambox Iris"
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris DisplayName "Streambox Iris"
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppRotateFiles 1
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppRotateOnline 0
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris Start SERVICE_DEMAND_START
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppRotateSeconds (New-Timespan -Days 1).TotalSeconds
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppParameters "-xml C:\ProgramData\Streambox\Iris\decoder.xml -logfile C:\ProgramData\Streambox\Iris\log\iris.log"
&"C:\Program Files\Streambox\Iris\nssm.exe" set StreamboxIris AppEvents Exit/Post 'C:\Windows\System32\windowspowershell\v1.0\powershell.exe -File ""C:\Program Files\Streambox\Iris\cleanlogs.ps1""'

try { $service = Get-Service -ErrorAction SilentlyContinue -Name StreamboxIris } catch {}
if (!$service) { throw "Can't configure StreamboxIris service" }

# enable all users to start/stop service
&C:\Windows\System32\sc.exe sdset $service "D:AR(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;IU)(A;;CCLCSWRPWPDTLOCRRC;;;BU)S:(AU;FA;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;WD)"

Update-HashPath -SourceDirectory 'C:\Streambox' -DestDirectory 'C:\ProgramData\Streambox\Iris\hash'
