function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = 'latest'


$url = "https://streambox-iris.s3-us-west-2.amazonaws.com/$version/win/streambox_iris_win.zip"

Set-Location C:\Windows\temp
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
If (Test-Path($filename)) { Remove-Item $filename }
Get-FileIfNotExists $url $filename

If ($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename))) {
    Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename
}

$directory = $fileinfo.Basename
$paths = @( Get-ChildItem -Recurse $directory -File | Select-Object -Expand Fullname )

If ($paths.Count -gt 1) {
    Write-Host $paths
    Throw "Found more files than expected.  Aborting..."
}

$installer = $paths | Select-Object -First 1

Write-Host $installer

&"$installer" /install /passive /log "irs_installer_${version}" | Out-String
