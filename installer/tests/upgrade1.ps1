Function doit() {
    Param([string]$wildcard,[switch]$uninstall)
    $installers = Get-ChildItem -Recurse "C:\ProgramData\Package Cache" -ea 0 | Select -Expand Fullname | Select-String -Pattern $wildcard

    for ($c=1; $c -lt $installers.Length+1; $c++){
        $installer = $installers[$c-1]
        $fileinfo = New-Object System.IO.FileInfo($installer)
        $log = "$($fileinfo.Basename)_uninstall_${c}.log"
        Write-Host $installer
        If($uninstall) {
             Write-Host $log
             &$installer /uninstall /passive /log $log | Out-String
        }
    }
}

function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = 'latest'
$url = "https://streambox-iris.s3-us-west-2.amazonaws.com/${version}/win/streambox_iris_win.zip"

cd C:\Windows\temp
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename

If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path $filename)){
    Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename
}

&"$($fileinfo.Basename)/iris.exe" /install /passive /log "iris_install_${version}.log" | Out-String


doit "iris"



$version = '0.5.3.1'
$log = "iris_install_${version}.log"
$url = "https://streambox-iris.s3-us-west-2.amazonaws.com/win/${version}/streambox_iris_win_${version}.zip"

cd C:\Windows\temp
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename

If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename))){
    Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename
}

&"$($fileinfo.Basename)/iris.exe" /install /passive /log $log | Out-String

doit "iris"
