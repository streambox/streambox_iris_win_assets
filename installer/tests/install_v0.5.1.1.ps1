function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = '0.5.1.1'
$log = "iris_install_${version}.log"
$url = "https://streambox-iris.s3-us-west-2.amazonaws.com/win/${version}/streambox_iris_win_${version}.zip"

cd C:\Windows\temp
$filename = "streambox_iris_${version}_win.zip"
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename

If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path $filename)){
    Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename
}

&"$($fileinfo.Basename)/iris.exe" /install /passive /log $log | Out-String
