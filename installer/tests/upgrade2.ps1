function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = 'latest'
$url = "https://streambox-iris.s3-us-west-2.amazonaws.com/${version}/win/streambox_iris_win.zip"

cd C:\Windows\temp
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename

If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path $filename)){
    Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename
}

&"$($fileinfo.Basename)/iris.exe" /install /passive /log "iris_install_${version}.log" | Out-String


$installers = Get-ChildItem -Recurse "C:\ProgramData\Package Cache" -ea 0 | Select -Expand Fullname | Select-String -Pattern "iris"
$installers








$wmipackages = Get-WmiObject -Class win32_product
$wmiproperties = gwmi -Query "SELECT ProductCode,Value FROM Win32_Property WHERE Property='UpgradeCode'"
$packageinfo = New-Object System.Data.Datatable
[void]$packageinfo.Columns.Add("Name")
[void]$packageinfo.Columns.Add("ProductCode")
[void]$packageinfo.Columns.Add("UpgradeCode")

foreach ($package in $wmipackages)
{
    $foundupgradecode = $false # Assume no upgrade code is found

    foreach ($property in $wmiproperties) {

        if ($package.IdentifyingNumber -eq $property.ProductCode) {
           [void]$packageinfo.Rows.Add($package.Name,$package.IdentifyingNumber, $property.Value)
           $foundupgradecode = $true
           break
        }
    }

    if(-Not ($foundupgradecode)) {
         # No upgrade code found, add product code to list
         [void]$packageinfo.Rows.Add($package.Name,$package.IdentifyingNumber, "")
    }
}

$packageinfo | Sort-Object -Property Name | Format-table ProductCode, UpgradeCode, Name >out1.txt


        
























$version = '0.5.1.1'
$log = "iris_install_${version}.log"
$url = "https://streambox-iris.s3-us-west-2.amazonaws.com/win/${version}/streambox_iris_win_${version}.zip"

cd C:\Windows\temp
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename

If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename))){
    Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename
}

&"$($fileinfo.Basename)/iris.exe" /install /passive /log $log | Out-String


$installers = Get-ChildItem -Recurse "C:\ProgramData\Package Cache" -ea 0 | Select -Expand Fullname | Select-String -Pattern "iris"
$installers
















$wmipackages = Get-WmiObject -Class win32_product
$wmiproperties = gwmi -Query "SELECT ProductCode,Value FROM Win32_Property WHERE Property='UpgradeCode'"
$packageinfo = New-Object System.Data.Datatable
[void]$packageinfo.Columns.Add("Name")
[void]$packageinfo.Columns.Add("ProductCode")
[void]$packageinfo.Columns.Add("UpgradeCode")

foreach ($package in $wmipackages)
{
    $foundupgradecode = $false # Assume no upgrade code is found

    foreach ($property in $wmiproperties) {

        if ($package.IdentifyingNumber -eq $property.ProductCode) {
           [void]$packageinfo.Rows.Add($package.Name,$package.IdentifyingNumber, $property.Value)
           $foundupgradecode = $true
           break
        }
    }

    if(-Not ($foundupgradecode)) {
         # No upgrade code found, add product code to list
         [void]$packageinfo.Rows.Add($package.Name,$package.IdentifyingNumber, "")
    }
}

$packageinfo | Sort-Object -Property Name | Format-table ProductCode, UpgradeCode, Name >out2.txt
